package com.example.todolist.di

import android.content.Context
import androidx.room.Room
import com.example.todolist.model.local.TodoDao
import com.example.todolist.model.local.TodoDatabase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
class DatabaseModule {

    @Provides
    fun providesTodoDao(todoDatabase: TodoDatabase): TodoDao {
        return todoDatabase.todoDao()
    }

    @Provides
    fun providesTodoDatabase(@ApplicationContext appContext: Context): TodoDatabase{
        return Room.databaseBuilder(appContext,TodoDatabase::class.java,"TodoDatabase").build()
    }
}
