package com.example.todolist.viewmodel

import com.example.todolist.model.local.entity.Todo

data class TodoState(
val isLoading: Boolean = false,
var todos: List<Todo> = emptyList(),
var selected: Boolean = false,
val errorMessage: String = ""
)
