package com.example.todolist.viewmodel

import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.todolist.model.TodoRepo
import com.example.todolist.model.local.entity.Todo
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject
import kotlinx.coroutines.Deferred
import kotlinx.coroutines.android.awaitFrame
import kotlinx.coroutines.async
import kotlinx.coroutines.awaitAll
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.filter
import kotlinx.coroutines.launch


@HiltViewModel
class MainViewModel @Inject constructor(private val repo: TodoRepo) : ViewModel() {

    private val _todos: MutableStateFlow<TodoState> =
        MutableStateFlow(TodoState())
    val todoState: StateFlow<TodoState> get() = _todos


    init {
        getTodos()
    }

    fun getTodos() = viewModelScope.launch{
        val call = viewModelScope.async {
            _todos.value = _todos.value.copy(todos = repo.getTodos()) }
        call.await()

    }

    fun selectTodo(selectingTodo: Todo) {
        with(_todos) {
            value = value.copy(selected = !value.selected)
            selectingTodo.isSelected = !selectingTodo.isSelected
        }

    }

    fun CreateTodo(todo: Todo) {
        _todos.value = _todos.value.copy(todos = _todos.value.todos + todo)
        viewModelScope.launch{
        repo.insertTodo(todo)
        repo.getTodos()
        }
        Log.e("%%%%%%%%%%","${_todos.value.todos}")
    }

    fun deleteTodo(todo: Todo){
        val newTodos = _todos.value.todos.filter { todos->
            todos != todo
        }
        _todos.value = _todos.value.copy(todos = newTodos)
        viewModelScope.launch {
            repo.deleteTodo(todo)
            repo.getTodos()}
    }


}