package com.example.todolist.model

    import com.example.todolist.model.local.TodoDao
    import com.example.todolist.model.local.entity.Todo
    import javax.inject.Inject

class TodoRepo @Inject constructor(private val todoDao: TodoDao) {

        suspend fun getTodos(): List<Todo>{
            return todoDao.getAllContacts()
        }

        suspend fun insertTodo(todo: Todo) {
            todoDao.insertTodo(todo)
        }

        suspend fun setIsComplete(todo: Todo){
            todoDao.updateIsComplete(todo)
        }

        suspend fun deleteTodo(todo: Todo){
            todoDao.deleteTodo(todo)
        }


    }


