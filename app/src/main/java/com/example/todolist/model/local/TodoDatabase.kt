package com.example.todolist.model.local

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.todolist.model.local.entity.Todo

@Database(entities = [Todo::class], version = 1)
abstract class TodoDatabase : RoomDatabase() {
    abstract fun todoDao(): TodoDao
}

