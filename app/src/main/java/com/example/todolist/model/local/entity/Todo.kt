package com.example.todolist.model.local.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "todos")
data class Todo(
    @PrimaryKey(autoGenerate = true) val id: Int = 0,
    val name: String = "",
    val description: String = "",
    @ColumnInfo(name = "is_selected")var isSelected: Boolean = false,
    @ColumnInfo(name = "is_complete")var completed: Boolean = false
)

