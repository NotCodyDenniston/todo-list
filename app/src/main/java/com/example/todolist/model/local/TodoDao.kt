package com.example.todolist.model.local

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update
import com.example.todolist.model.local.entity.Todo

@Dao
interface TodoDao {

    @Query("SELECT * FROM todos")
    suspend fun getAllContacts(): List<Todo>

    @Insert
    suspend fun insertTodo(todo: Todo)

    @Update
    suspend fun updateIsComplete(todo: Todo)

    @Delete
    suspend fun deleteTodo(todo: Todo)
}
