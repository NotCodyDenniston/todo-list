package com.example.todolist.view.screens

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.text.BasicTextField
import androidx.compose.material3.Button
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.platform.ViewCompositionStrategy
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.findNavController
import com.example.todolist.R
import com.example.todolist.model.local.entity.Todo
import com.example.todolist.ui.theme.TodoListTheme
import com.example.todolist.viewmodel.MainViewModel

class CreateTodoFragment : Fragment() {
    val mainViewModel: MainViewModel by activityViewModels()
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return ComposeView(requireContext()).apply {
            setViewCompositionStrategy(ViewCompositionStrategy.DisposeOnViewTreeLifecycleDestroyed)
            setContent {
                TodoListTheme() {
                    Column(
                        horizontalAlignment = Alignment.CenterHorizontally,
                        verticalArrangement = Arrangement.Top) {
                        Text(text = " Create Todo ",
                            color = Color.Black,
                            fontSize = 30.sp,
                            textAlign = TextAlign.Center,
                            modifier = Modifier.background(color = Color(0xffC2FFD6))
                                .fillMaxWidth()
                                .padding(vertical = 30.dp))
                        CreateTodoScreen(
                            navigate = {findNavController().navigate(R.id.dashboard_fragment)},
                            saveTodo = {todo-> mainViewModel.CreateTodo(todo) }
                        )
                    }
                }
            }
        }
    }
}

@Composable
fun CreateTodoScreen(navigate: ()->Unit, saveTodo:(Todo)->Unit){
    val _todo = remember {
        mutableStateOf(Todo())
    }

    Column(
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.Top,
        modifier = Modifier.padding(vertical = 20.dp)) {

        BobsTextField(value = _todo.value.name, name = "Name"){  newName->
            _todo.value = _todo.value.copy(name = newName)
        }
        BobsTextField(value = _todo.value.description, name = "Description"){ newDescription->
            _todo.value = _todo.value.copy(description = newDescription)
        }


    Button(onClick = {
        saveTodo(_todo.value)
        navigate()
    }) {
        Text(text = "Create")
    }
    }
}

@Composable
fun BobsTextField(value:String,name:String, onTextChange:(String)->Unit){
    Text(text = name)
    BasicTextField(
        value = value,
        onValueChange = {onTextChange(it)},
        modifier = Modifier.background(color = Color.LightGray))
}