package com.example.todolist.view.screens

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material3.Button
import androidx.compose.material3.Snackbar
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.platform.ViewCompositionStrategy
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.findNavController
import com.example.todolist.R
import com.example.todolist.model.local.entity.Todo
import com.example.todolist.ui.theme.TodoListTheme
import com.example.todolist.viewmodel.MainViewModel

class DashboardFragment: Fragment() {
    val mainViewModel:MainViewModel by activityViewModels()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        mainViewModel.getTodos()
        return ComposeView(requireContext()).apply {
            setViewCompositionStrategy(ViewCompositionStrategy.DisposeOnViewTreeLifecycleDestroyed)
            setContent {
                val todoState = mainViewModel.todoState.collectAsState().value
                TodoListTheme() {
                    Column(
                        horizontalAlignment = Alignment.CenterHorizontally,
                        verticalArrangement = Arrangement.Top) {
                        Text(text = " Todo Dashboard ",
                            color = Color.White,
                            fontSize = 30.sp,
                            textAlign = TextAlign.Center,
                            modifier = Modifier
                                .background(color = Color(0xffDB6C79))
                                .fillMaxWidth()
                                .padding(vertical = 30.dp))
                        DashboardScreen(
                            navigate = {findNavController().navigate(R.id.create_fragment)},
                            todos = todoState.todos,
                            delete = {todo-> mainViewModel.deleteTodo(todo)},
                            selectTodo = {todo-> mainViewModel.selectTodo(todo)}
                        )
                    }
                }
            }
        }
    }


}


@Composable
fun DashboardScreen(navigate:()->Unit, todos: List<Todo>, selectTodo:(Todo)->Unit, delete:(Todo)->Unit){

        Column(
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.Top,
        modifier = Modifier.padding(vertical = 20.dp)) {
                Text(text = "Tasks:")
            if(todos.isEmpty()){
                Snackbar(modifier = Modifier){
                   Column(modifier = Modifier.fillMaxWidth(), horizontalAlignment = Alignment.CenterHorizontally) {
                Text(text = "🥳🥳All your todos are done!!🥳🥳", textAlign = TextAlign.Center)
                    }
                }
            }
        LazyColumn(
            horizontalAlignment = Alignment.CenterHorizontally,
            verticalArrangement = Arrangement.Top){
            items(todos){ todo->
                Text(text = "Name: ${todo.name}",
                    modifier = Modifier
                        .clickable {
                            selectTodo(todo)
                        }
                        .padding(5.dp)
                        .background(color = Color(0xff004777)),
                        color = Color.White,
                        textAlign = TextAlign.Left)
                if (todo.isSelected){
                Text(text = "Description: ${todo.description}")
                Button(onClick = {
                    todo.completed = !todo.completed
                    delete(todo)}) {
                    Text(text = "Completed: ${todo.completed}")
                    }
                }
            }
        }
    Button(onClick = { navigate() }) {
        Text(text = "Add a Todo!")
    }
    }
}